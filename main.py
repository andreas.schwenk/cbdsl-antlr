# ------------------------------------------------------------------------------
# ANTLR-Example by Andreas Schwenk, www.compiler-construction.com
# MIT License
# ------------------------------------------------------------------------------
# This is the main file that calls the parser, interpreter and code generator.
# ------------------------------------------------------------------------------

import os, sys, pprint

import listener
import model

if __name__== "__main__":

	if sys.version_info[0] < 3:
		raise Exception("You have to use Python version 3 ($python3)!")

	if len(sys.argv) != 2:
		print("usage: python3 main.py inputpath")
		sys.exit(-1)

	inputpath = sys.argv[1]

	# ----- parse and populate domain model -----
	program = listener.parse(inputpath)

	# ----- interpret program -----
	print('\n----- interpretation -----')
	program.interpret()
	print('----- end of interpretation -----')

	# ----- generate code and print generated code -----
	generated_code = program.generate()
	print('\n----- generated code (lingua fantastica) -----')
	print(generated_code)
	print('----- end of generated code -----')


	print("[SUCCESS]")
