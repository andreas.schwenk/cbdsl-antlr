# ------------------------------------------------------------------------------
# ANTLR-Example by Andreas Schwenk, www.compiler-construction.com
# MIT License
# ------------------------------------------------------------------------------
# This file defines the domain model including interpretation and code
# generation. For understanding, it might be helpful that you draw a (coarse)
# class diagram with all associations.
# ------------------------------------------------------------------------------

import sys

class Program:

	def __init__(self):
		self.parts = []       # list of instances of class Part
		self.variables = []   # list of instances of class Variable

	def interpret(self):
		for part in self.parts:
			if part.type == 'math':
				part.ref.interpret(self)
				# the 1st parameter is a program (self), since we need a
				# reference to the set of variables

	def generate(self):
		s = ''
		for part in self.parts:
			s += part.generate()
		return s

	def getVariable(self, identifier):
		for v in self.variables:
			if v.identifier == identifier:
				return v
		return None


class Part:

	def __init__(self, type):
		self.type = type      # 'text' | 'math'
		self.ref = None       # reference to class Text or class Expression

	def generate(self):
		return self.ref.generate()


class Text:

	def __init__(self):
		self.string = ''

	def generate(self):
		return 'print("' + self.string + '")\n'


class Expression:

	def __init__(self):
		# expression stack is given in postfix notation:
		# example:   a=(3+4)*2  ->  [ 'a' '3' '4' '*' '2' '=' ]
		# note that we only store strings; we should better create classes
		# Operand, Operator, Constant, ...
		self.postfix = []

	def isOperator(self, s):
		return s=='+' or s=='-' or s=='*' or s=='/'
	def isConstant(self, s):
		return s[0]>='0' and s[0]<='9'
	def isIdentifier(self, s):
		if isinstance(s, str) == False:  # if "str" is a number, then we must quit here
			return False
		return (s[0]>='A' and s[0]<='Z') or (s[0]>='a' and s[0]<='z')

	def interpret(self, program):
		# (note that this method does not perform "all" tests)
		stack = []
		for token in self.postfix:
			# constants are pushed to the stack
			if self.isConstant(token):
				stack.append(  float(token)  )
			# variables are pushed to the stack
			elif self.isIdentifier(token):
				stack.append(  token  )
			# operators pop the two top-most tokens from the stack,
			# perform the operation and push the result back to the stack
			elif self.isOperator(token):
				# get operands
				rightOperand = stack.pop()
				leftOperand = stack.pop()
				# read variable value, if necessary (otherwise, it is a constant value)
				if self.isIdentifier(leftOperand):
					variable = program.getVariable(leftOperand)
					if variable == None:
						print('interpreter-error: unknown variable ' + token)
						sys.exit(-1)
					leftOperand = variable.value
				# read variable value, if necessary (otherwise, it is a constant value)
				if self.isIdentifier(rightOperand):
					variable = program.getVariable(rightOperand)
					if variable == None:
						print('interpreter-error: unknown variable ' + token)
						sys.exit(-1)
					rightOperand = variable.value
				# perform operation
				if token=='+':
					result = leftOperand + rightOperand
				elif token=='-':
					result = leftOperand - rightOperand
				elif token=='*':
					result = leftOperand * rightOperand
				elif token=='/':
					result = leftOperand / rightOperand
				else:
					print('interpreter-error: unexpected operator ' + token)
					sys.exit(-1)
				stack.append(result)    # push intermediate result to the stack
			# an assignment pops the two top-most tokens from the stack,
			# checks, wheather the left-hand side is a variable and the
			# right-hand side is a value
			# and then assigs the right-hand side to the left-hand side
			elif token=='=':
				rhs = stack.pop()  # right-hand side
				lhs = stack.pop()  # left-hand side
				if self.isIdentifier(lhs):
					variable = program.getVariable(lhs)
					if variable==None:
						# create a new variable, if it does not yet exist
						variable = Variable(lhs)
						program.variables.append(variable)
					variable.value = rhs
				stack.append(variable.identifier)  # this is always the last operation for our purposes, since we forbid e.g. "a+(b=c)" by our grammar
			else:
				print('interpreter-error: unexpected token ' + token)
				sys.exit(-1)
		# print
		if len(stack) != 1:
			print('interpreter-error: stack has incorrect length')
			sys.exit(-1)
		tos = stack[0]   # tos := top of stack
		if self.isIdentifier(tos):
			variable = program.getVariable(tos)
			if variable == None:
				print('interpreter-error: variable ' + tos)
				sys.exit(-1)
			print(variable.identifier + '=' + str(variable.value))
		else:
			print('ans=' + str(tos))

	def generate(self):
		# generation is simple, since we store the expressions in postfix notation anyway
		s = ''
		for token in self.postfix:
			s += str(token) + ' '
		return 'calc(' + s.strip() + ')' + '\n' # strip() removes the last space before ')'


class Variable:

	def __init__(self, identifier):
		self.identifier = identifier
		self.value = 0
