# ANTLR Example

ANTLR-Example (Python) for my lecture CBDSL. Also refer to: [www.compiler-construction.com](www.compiler-construction.com).

The code focusses on parsing, interpretation and code generation.

*I am thankful to any kind of feedback. Please send a mail to [errata@compiler-construction.com](mailto:errata@compiler-construction.com).*

*Please note, that the focus is on ANTLR: the implementation itself is not perfect (e.g. I do not catch every feasible exception).*

## Installation (Linux)

**Step 1:** Install [Python 3](https://www.python.org/download/releases/3.0/)

**Step 2:** Install ANTLR4:

* Linux:
``
    sudo apt-get install antlr4
``

* macOS (using [Homebrew](https://brew.sh)):
``
    brew install antlr@4
``

* Windows:
[https://www.antlr.org/download.html](https://www.antlr.org/download.html):
Click on "ANTLR tool itself" and copy the jar file into the directory of your grammar file (`mydsl.g4`)

**Step 3:** Install ANTLR4 runtime for Python:

``
    pip3 install antlr4-python3-runtime
``

## Run this Project

**Step 1:** Rebuild the grammar:

* Linux:
``
    antlr4 -Dlanguage=Python3 mydsl.g4
``

* macOS:
``
    antlr -Dlanguage=Python3 mydsl.g4
``

* Windows:
``
    java -jar antlr-4.7.2-complete.jar -Dlanguage=Python3 mydsl.g4
``
(your may have to replace the version number)

**Step 2:** Run the compiler:

``
python3 main.py mydslcode.txt
``

## Description of files provided

* `mydslcode.txt`: Example code of the custom DSL
* `mydsl.g4`: Grammar definition
* `main.py`: Main python file
* `listener.py`: Methods called while parsing
* `model.py`: Entity class for the domain-model / abstract syntax tree
* `build-and-run.sh`: Shell script that can optionally be called to build the grammar and run the example. You may need to set permissions via `chmod u+x build-and-run.sh` first.
 
Note that ANTLR generates more files. These are listed in the `.gitignore` file, such that `git` does not synchronize these files.

## External documentation

* [https://www.antlr.org](https://www.antlr.org)

## Example run

Content of `mydslcode.txt`:

```
Hallo, I am a calculator, solving every line that starts with a dollar sign.

$ 1 + 1

It is also feasible to assign values to a variable:

$ a = (3 + 4) * 3

Another example:

$ b = a * 2

All calculation tasks are (a) interpreted and (b) written as code in postfix notation.
```

The DSL file is here both interpreted and compiled. Usually only one option is required.

* Interpreted output:

```
ans=2
a=21
b=42
```

* Compiled output / generated code:

```
print("Hello, I am a calculator, solving every line that starts with a dollar sign.")
calc(1 1 +)
print("It is also feasible to assign values to a variable:")
calc(a 3 4 + 3 * =)
print("Another example:")
calc(b a 2 * =)
print("All calculation tasks are (a) interpreted and (b) written as code in postfix notation.")
```

*Note: the target langauge is a fantasy language. It expects all expressions to be in [postfix notation](https://en.wikipedia.org/wiki/Reverse_Polish_notation).*
