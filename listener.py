# ------------------------------------------------------------------------------
# ANTLR-Example by Andreas Schwenk, www.compiler-construction.com
# MIT License
# ------------------------------------------------------------------------------
# This file defines the listener that is used to populate your domain model.
# ------------------------------------------------------------------------------

import antlr4, os, sys

from mydslLexer import mydslLexer
from mydslParser import mydslParser
from mydslListener import mydslListener
from antlr4.error.ErrorListener import ErrorListener

import model

currentInputFile = ''


def parse_error(msg):
	print("ERROR: " + msg)
	sys.exit(-1)


class mydslListenerX(mydslListener):

	def __init__(self, program):
		self.program = program   # domain model: class model.Program
		self.part = None         # class model.Part
		self.text = None         # parse model.text
		self.expression = None

	# method prototypes can be found in file "mydslListener.py",
	# which is generated by ANTLR. You need to run
	# "antlr -Dlanguage=Python3 mydsl.g4" after editing "mydsl.g4"!

	# note the difference between prefixes "enter" and "exit"!

	def enterTextline(self, ctx:mydslParser.TextlineContext):
		self.text = model.Text()
		self.part = model.Part('text')
		self.program.parts.append( self.part )
		self.part.ref = self.text

	def exitTextitem(self, ctx:mydslParser.TextitemContext):
		assert(self.text != None)
		item = ctx.getText()
		if item != '.' and item != ',' and item != '(' and item != ')' and item != ':' and len(self.text.string)>0:
			self.text.string += ' '
		self.text.string += item

	def enterMathline(self, ctx:mydslParser.MathlineContext):
		self.expression = model.Expression()
		self.part = model.Part('math')
		self.program.parts.append( self.part )
		self.part.ref = self.expression

	def enterAssignment(self, ctx:mydslParser.AssignmentContext):
		assert(self.expression != None)
		variableIdentifier = ctx.ID().getText()
		self.expression.postfix.append(variableIdentifier)

	def exitAssignment(self, ctx:mydslParser.AssignmentContext):
		assert(self.expression != None)
		self.expression.postfix.append('=')

	def exitAddition(self, ctx:mydslParser.AdditionContext):
		assert(self.expression != None)
		operator = ctx.add_sub_operator()
		if operator != None:
			 # the second part of the rule "additoin" has no operator!
			self.expression.postfix.append( operator.getText() )

	def exitMultiplication(self, ctx:mydslParser.MultiplicationContext):
		assert(self.expression != None)
		operator = ctx.mul_div_operator()
		if operator != None:
			 # the second part of the rule "additoin" has no operator!
			self.expression.postfix.append( operator.getText() )

	def exitOperand_id(self, ctx:mydslParser.Operand_idContext):
		assert(self.expression != None)
		self.expression.postfix.append(ctx.getText())

	def exitOperand_constant(self, ctx:mydslParser.Operand_constantContext):
		assert(self.expression != None)
		self.expression.postfix.append(ctx.getText())


class MyErrorListener( ErrorListener ):

	def syntaxError(self, recognizer, offendingSymbol, line, column, msg, e):
		msgstr = str(msg)

		print("SyntaxError:" + currentInputFile + ":" + str(line) + ":" + str(column+1) + ": " + msgstr)
		print("[ERRORS]")
		sys.exit(-1)


def parse(inputpath):

	global currentInputFile
	currentInputFile = inputpath

	if os.path.exists(inputpath)==False:
		parse_error("input file '" + inputpath + "' does not exist")

	with open(inputpath, encoding="utf-8") as f:
		lines = f.readlines()

	source = ''
	for line in lines:
		source += line

	#print('------ DEBUG INFO: SOURCE -----')
	#print(source)
	#print('------ END OF DEBUG INFO -----')

	inputStream = antlr4.InputStream(source);
	lexer = mydslLexer(inputStream)
	stream = antlr4.CommonTokenStream(lexer)

	parser = mydslParser(stream)
	parser._listeners = [ MyErrorListener() ]

	root = parser.program()   # get the root rule "program" (refer to mydsl.g4)

	program = model.Program() # create the domain model (refer to file "model.py")

	listener = mydslListenerX(program)

	walker = antlr4.ParseTreeWalker()
	walker.walk(listener, root)

	return program
