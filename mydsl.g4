/*
  ANTLR-Example by Andreas Schwenk, www.compiler-construction.com
  MIT License

  This grammar file consists of a set of rules.
  It can be compiled with "antlr -Dlanguage=Python3 mydsl.g4".

  A rule has the following form:

    - starts with an ID and is followed by a colon (':')
    - ends with a semicolon (';')
    - in between:

        - '-'        a terminal symbol '-'
        - id         a reference to another rule (=nonterminal)
        - seq        a sequence of nonterminals/terminals
        - ( seq )*   repetition of a sequence of terminals/nonterminals: 0..inf
        - ( seq )+   repetition with 1..inf iterations
        - ( seq )?   the sequence may occur or not (optional)

*/

grammar mydsl;

program
	: ( statement )*
	  EOF
	;

statement
	: textline ( '\n' )+
	| mathline  ( '\n' )+
	;

textline
	: textitem+
	;

textitem
	: ID | ',' | '.' | ';' | ':' | '(' | ')'
	;

mathline
	: '$' assignment
	| '$' addition
	;

assignment
	: ID '=' addition
	;

addition
	: addition add_sub_operator multiplication
	| multiplication
	;

add_sub_operator
	: '+'
	| '-'
	;

multiplication
	: multiplication mul_div_operator operand
	| operand
	;

mul_div_operator
	: '*'
	| '/'
	;

operand
	: operand_id
	| operand_constant
	| operand_parentheses
	;

operand_id
	: ID
	;

operand_constant
	: INTEGER
	;

operand_parentheses
	: '(' addition ')'
	;


/* --- the following rules can be reused in many other projects --- */

ID
	: ID_START ID_CHAR*
	;

fragment ID_START
	: ('a' .. 'z') | ('A' .. 'Z') | '_'
	;

fragment ID_CHAR
	: ID_START | ('0' .. '9')
	;

INTEGER
	: DIGIT DIGIT0*
	| DIGIT0
	;

PATTERN
 	: ',' | ':' | ';' | '/' | '.'
 	;

STRING
	: '\'' (~['"] )* '\''
	;

fragment DIGIT0
	: '0' .. '9'
	;

fragment DIGIT
	: '1' .. '9'
	;

WS   /* white spaces; are skipped; you may add '\n' for C-like langauges */
	: [ \t\r]+ -> skip
	;
